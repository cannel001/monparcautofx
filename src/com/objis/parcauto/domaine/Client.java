package com.objis.parcauto.domaine;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Client {
	
	//les proprietes
	private ObjectProperty<Long> id;
	private StringProperty nom;
	private StringProperty prenom;
	private StringProperty telephone;
	private StringProperty sexe;
	
	//les constructeurs
	//par defaut
	public Client() {
		super();
	}

	//avec parametres
	public Client(Long id, String nom, String telephone, String sexe,String prenom) {
		this.id = new SimpleObjectProperty<Long>(id);
		this.nom = new SimpleStringProperty(nom);
		this.telephone = new SimpleStringProperty(telephone);
		this.sexe = new SimpleStringProperty(sexe);
		this.prenom=new SimpleStringProperty(prenom);
	}

	//les getters et setters
	//id
	public ObjectProperty<Long> idProperty() {
		return id;
	}
	
	public Long getId() {
		return id.get();
	}
	
	public void setId(Long id) {
		this.id.set(id);
	}

	//nom
	public StringProperty nomProperty() {
		return nom;
	}
	
	public String getNom() {
		return nom.get();
	}
	
	public void setNom(String nom) {
		this.nom.set(nom);
	}

	//telephone
	public StringProperty telephoneProperty() {
		return telephone;
	}
	
	public String getTelephone() {
		return telephone.get();
	}
	
	public void setTelephone(String telephone) {
		this.telephone.set(telephone);
	}

	//sexe
	public StringProperty sexeProperty() {
		return sexe;
	}
	
	public String getSexe() {
		return sexe.get();
	}
	
	public void setSexe(String sexe) {
		this.sexe.set(sexe);
	}
	
	//prenom
	public void setPrenom(String prenom) {
		this.prenom.set(prenom);
	}
	
	public String getPrenom() {
		return prenom.get();
	}
	
	public StringProperty prenomProperty() {
		return prenom;
	}

	
	//methode de description ToString
	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", telephone=" + telephone + ", sexe=" + sexe + "]";
	}
	
	
	
	
	

}
