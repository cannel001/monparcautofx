package com.objis.parcauto.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Singleton {
	
	//les proprietes
	private static Connection connx;
	//parametre de connexion
	private final static String URL="Jdbc:Mysql://localhost:3306/bd_parcauto";
	private final static String LOGIN="root";
	private final static String PASS="";
	
	private Singleton() {
		try {
			connx=DriverManager.getConnection(URL,LOGIN,PASS);
		} catch (SQLException e) {
			
			Alert alert=new Alert(AlertType.ERROR);
			alert.setHeaderText("CONNEXION ECHOUEE");
			alert.setContentText("Connexion � la base de donn�es echou�e. Veuillez verifier vos parametres de connexion svp");
			alert.showAndWait();
			
			System.exit(0);
			
			e.printStackTrace();
			
			
		}
	}
	
	public static Connection getInstance() {
		
		if (connx==null) {
			new Singleton();
			return connx;
		}
		
		return connx;
		
	}
	
	

}
