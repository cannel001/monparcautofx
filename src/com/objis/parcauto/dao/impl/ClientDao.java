package com.objis.parcauto.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.objis.parcauto.domaine.Client;

public class ClientDao extends Dao<Client,Long> {
	
	//les proprietes
	private final static String createStatement="insert into client (nom,prenom,sexe,telephone) values (?,?,?,?)";
	private final static String readOneStatement="select * from client where id=?";
	private final static String readAllStatement="select * from client";
	private final static String updateStatement="update client set nom=?,sexe=?,telephone=?,prenom=? where id=?";
	private final static String deleteStatement="delete from client where id=?";
	private final static String findLaststatement="select * from client order by id desc limit 1";
	
	private int executeQuery=-1;

	@Override
	public Boolean create(Client t) {
		
		try {
			
			createStatement(createStatement);
			
			ps.setString(1, t.getNom());
			ps.setString(2, t.getPrenom());
			ps.setString(3, t.getSexe());
			ps.setString(4, t.getTelephone());
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	@Override
	public Client readOne(Long pk) {
		
		Client client=null;
		
		createStatement(readOneStatement);
		
		try {
			
			ps.setLong(1, pk);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				
				client=new Client(rs.getLong("id"), rs.getString("nom"), rs.getString("telephone"), rs.getString("sexe"), rs.getString("prenom"));
				
				return client;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<Client> readAll() {
		
		List<Client> listeClient=new LinkedList<>();
		
		try {
			createStatement(readAllStatement);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				
				listeClient.add(new Client(rs.getLong("id"), rs.getString("nom"), rs.getString("telephone"), rs.getString("sexe"), rs.getString("prenom")));
				
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listeClient;
	}

	@Override
	public Boolean update(Client t) {
		
		try {
			
			createStatement(updateStatement);
			
			ps.setString(1,t.getNom());
			ps.setString(2, t.getSexe());
			ps.setString(3, t.getTelephone());
			ps.setString(4, t.getPrenom());
			ps.setLong(5, t.getId());
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	@Override
	public Boolean delete(Long pk) {
		
		try {
			
			createStatement(deleteStatement);
			
			ps.setLong(1, pk);
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}
	
	/**
	 * methode permettant de retourner le dernier enregistrement
	 */
	public Client findLastElement() {
		
		Client client = null;
		
		try {
			
			createStatement(findLaststatement);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				client=new Client(rs.getLong("id"), rs.getString("nom"), rs.getString("telephone"), rs.getString("sexe"), rs.getString("prenom"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return client;
	}
	
	

}
