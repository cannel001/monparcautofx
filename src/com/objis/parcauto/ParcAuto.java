package com.objis.parcauto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.objis.parcauto.domaine.Chauffeur;
import com.objis.parcauto.domaine.Client;
import com.objis.parcauto.domaine.Garage;
import com.objis.parcauto.domaine.Voiture;
import com.objis.parcauto.service.impl.ChauffeurService;
import com.objis.parcauto.service.impl.ClientService;
import com.objis.parcauto.service.impl.GarageService;
import com.objis.parcauto.service.impl.VoitureService;
import com.objis.parcauto.vue.VueChauffeurControlleur;
import com.objis.parcauto.vue.VueClientControlleur;
import com.objis.parcauto.vue.VueGarageControlleur;
import com.objis.parcauto.vue.VueMenuNavigationControlleur;
import com.objis.parcauto.vue.VueMenuPrincipalControlleur;
import com.objis.parcauto.vue.VueVoitureControlleur;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ParcAuto extends Application {
	
	//les proprietes
	private static Stage primaryStage=new Stage();
	private static BorderPane VueMenuPrincipal=new BorderPane();
	
	private ChauffeurService chauffeurService=new ChauffeurService();
	private ClientService clientService=new ClientService();
	private GarageService garageService=new GarageService();
	private VoitureService voitureService=new VoitureService();
	
	private ObservableList<Chauffeur> listChauffeur=FXCollections.observableArrayList();
	private ObservableList<Client> listClient=FXCollections.observableArrayList();
	private ObservableList<Garage> listGarage=FXCollections.observableArrayList();
	private ObservableList<Voiture> listVoiture=FXCollections.observableArrayList();
	
	
	//constructeur
	public ParcAuto() {
		
		//chargement de la liste des chauffeurs
		listChauffeur.addAll(chauffeurService.readAll());
		
		//chargement de la liste des clients
		listClient.addAll(clientService.readAll());
		
		//chargement de la liste des garages
		listGarage.addAll(garageService.readAll());
		
		//chargement de la liste des voitures
		listVoiture.addAll(voitureService.readAll());
		
		//generationDocument();

	}

	@Override
	public void start(Stage primaryStage) {
		
		//afficher la vue principal
		afficherVueMenuPrincipal();
		
		//afficher vueNavugation � droite
		afficherVueMenuNavigation();
		
		//afficher vueAccueil au centre
		afficherAccueil();
		
		
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * methode pour afficher la vue menu principal
	 */
	public void afficherVueMenuPrincipal() {
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(getClass().getResource("vue/VueMenuPrincipal.fxml"));
		try {
			VueMenuPrincipal=(BorderPane)loader.load();
			Scene scene=new Scene(VueMenuPrincipal);
			primaryStage.setScene(scene);
			primaryStage.setTitle("MENU PRINCIPAL");
			primaryStage.show();
			primaryStage.setMaximized(true);
			
			VueMenuPrincipalControlleur controlleur=loader.getController();
			controlleur.setParcAuto(this);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * methode pour afficher le menuNavigation
	 */
	public void afficherVueMenuNavigation() {
		AnchorPane vueMenunavigation=new AnchorPane();
		
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(getClass().getResource("vue/VueMenuNavigation.fxml"));
		
		try {
			vueMenunavigation=(AnchorPane)loader.load();
			
			VueMenuPrincipal.setLeft(vueMenunavigation);
			
			VueMenuNavigationControlleur controller=loader.getController();
			
			controller.setParAuto(this);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * methode pour afficher l'acceuil
	 * 
	 */
	public void afficherAccueil() {
		AnchorPane vueAccueil=new AnchorPane();
		
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(getClass().getResource("vue/VueAccueil.fxml"));
		
		try {
			vueAccueil=(AnchorPane)loader.load();
			VueMenuPrincipal.setCenter(vueAccueil);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * methode pour changer de vue au centre du stage principal
	 */
	public void changeVueCentre(String url) {
		AnchorPane vueCentre=new AnchorPane();
		
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(getClass().getResource(url));
		
		try {
			vueCentre=(AnchorPane)loader.load();
			VueMenuPrincipal.setCenter(vueCentre);
			
			if (url=="vue/VueChauffeur.fxml") {
				VueChauffeurControlleur controlleur=loader.getController();
				controlleur.setParAuto(this);
			}
			else if(url=="vue/VueClient.fxml"){
				VueClientControlleur controlleur=loader.getController();
				controlleur.setParcAuto(this);
			}
			else if(url=="vue/VueGarage.fxml") {
				VueGarageControlleur controlleur=loader.getController();
				controlleur.setParAuto(this);
			}
			else if(url=="vue/VueVoiture.fxml") {
				VueVoitureControlleur controlleur=loader.getController();
				controlleur.setParcAuto(this);
			}
			
			
			
			primaryStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * methode pour retourner la liste de chauffeur
	 */
	public ObservableList<Chauffeur> getListChauffeur(){
		return listChauffeur;
	}
	
	/**
	 * methode pour retourner la liste des clients
	 */
	public ObservableList<Client> getListClient(){
		return listClient;
	}
	
	/**
	 * methode pour retourner la liste des garages
	 */
	public ObservableList<Garage> getListGarage(){
		return listGarage;
	}
	
	/**
	 * methode pour retourner la liste des voitures
	 */
	public ObservableList<Voiture> getListVoiture(){
		return listVoiture;
	}
	
	
	//generer un document avec jasper report
	public void generationDocument() {
		
		//flux de sortie
		try {
			
			//recuperation du dossier de reception du fichier
			String userDirectory=System.getProperty("user.home");
			System.out.println(userDirectory);
			
			//fichier du flux de sortie
			String outputFile=userDirectory+File.separator+"jasper.pdf";
			System.out.println(outputFile);
			
			List<Voiture> testlist=new LinkedList<>();
			testlist.add(new Voiture(null, null, null, "hjghj", "gjgjgj", "gjgjh"));
			
			//convertion de la liste voiture voiture collection jasperReport
			JRBeanCollectionDataSource dataSourceVoiture=new JRBeanCollectionDataSource(testlist);
			
			System.out.println("ok 1");
			
			//parametres
			Map<String, Object> enregistrementCarDataSource=new HashMap<>();
			enregistrementCarDataSource.put("enregistrementVoiture", dataSourceVoiture);
			System.out.println("ok2");
			
			//generation
			JasperPrint jasperPrint=JasperFillManager.fillReport("ressources/listeCar.jrxml", enregistrementCarDataSource);
			System.out.println("ok3");
			
			//flux de sortie du pdf
			OutputStream stream=new FileOutputStream(new File(outputFile));
			System.out.println("ok4");
			
			//exporter le document
			JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			System.out.println("ok5");
			
			System.out.println("fichier cr�e");
			
		} catch (FileNotFoundException | JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	
	/**
	 * methode pour retourner le stage en cours
	 */
	public Stage getStage() {
		return primaryStage;
	}	
	
}
