package com.objis.parcauto.vue;

import com.objis.parcauto.ParcAuto;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class VueMenuNavigationControlleur {
	
	//les proprietes
	private ParcAuto parcAuto=new ParcAuto();
	
	private Stage stage;
	
	@FXML
	private Button btnVoiture=new Button();
	@FXML
	private Button btnGarage=new Button();
	@FXML
	private Button btnClient=new Button();
	@FXML
	private Button btnChauffeur=new Button();
	
	@FXML
	public void initialize() {
		
	}
	
	/**
	 * methode pour afficher le menu voiture lorsqu'on click sur le bouton voiture
	 */
	@FXML
	private void clickSurBtnVoiture() {
		parcAuto.changeVueCentre("vue/VueVoiture.fxml");
	}
	
	
	
	/**
	 * methode pour afficher le menu chauffeur lorsqu'on click sur le bouton chauffeur
	 */
	@FXML
	private void clickSurBtnChauffeur() {
		parcAuto.changeVueCentre("vue/VueChauffeur.fxml");
		
		
	}
	
	/**
	 * methode pour afficher le menu client lorsqu'on click sur le bouton client
	 */
	@FXML
	private void clickSurBtnClient() {
		parcAuto.changeVueCentre("vue/VueClient.fxml");
	}
	
	/**
	 * methode pour afficher le menu garage lorsqu'on click sur le bouton garage
	 */
	@FXML
	private void clickSurBtnGarage() {
		parcAuto.changeVueCentre("vue/VueGarage.fxml");
	}
	
	/**
	 * methode pour revenir au menu accueil lorqu'on click sur le bouton accueil
	 */
	@FXML
	private void clickSurBtnAccueil() {
		parcAuto.changeVueCentre("vue/VueAccueil.fxml");
	}
	
	public void setStage(Stage stage) {
		this.stage=stage;
	}
	
	public void setParAuto(ParcAuto auto) {
		this.parcAuto=auto;
	}
	

}
