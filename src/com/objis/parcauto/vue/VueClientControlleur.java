package com.objis.parcauto.vue;

import java.util.Optional;

import com.objis.parcauto.ParcAuto;
import com.objis.parcauto.domaine.Client;
import com.objis.parcauto.service.impl.ClientService;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class VueClientControlleur {
	
	//les proprietes
	private ParcAuto auto;
	private ClientService service=new ClientService();
	private Alert alert;
	
	@FXML
	private TextField txfNom;
	@FXML
	private TextField txfPrenom;
	@FXML
	private TextField txfTel;
	@FXML
	private TextField txfSexe;
	
	@FXML
	private TableView<Client> tableClient;
	
	@FXML
	private TableColumn<Client, String> columNom;
	@FXML
	private TableColumn<Client, String> columPrenom;
	@FXML
	private TableColumn<Client, String> columTel;
	@FXML
	private TableColumn<Client, String> columSexe;
	
	@FXML
	private Button btnAjouter;
	@FXML
	private Button btnModifier;
	@FXML
	private Button btnSupprimer;
	@FXML
	private Button btnReset;
	
	@FXML
	private void initialize() {
		
		initChamp();
		
		columNom.setCellValueFactory(e->e.getValue().nomProperty());
		columPrenom.setCellValueFactory(e->e.getValue().prenomProperty());
		columSexe.setCellValueFactory(e->e.getValue().sexeProperty());
		columTel.setCellValueFactory(e->e.getValue().telephoneProperty());
		
		//remplir les champs de saisies lorsqu'on click sur une ligne du tableau
		tableClient.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->changeValuesChamps(newValue));
		
		//Etat des boutons par defaut
		btnAjouter.setDisable(false);
		btnModifier.setDisable(true);
		btnSupprimer.setDisable(true);
		btnReset.setDisable(true);
		
	}
	
	/**
	 * methode permettant de modfier le ParcAUto
	 */
	public void setParcAuto(ParcAuto auto) {
		this.auto=auto;
		tableClient.setItems(auto.getListClient());
	}
	
	/**
	 * methode permettant de changer les valeurs des champs de saises � chaque fois qu'on selectionne une ligne du tableau
	 */
	private void changeValuesChamps(Client client) {
		
		if (client!=null) {
			
			txfNom.setText(client.getNom());
			txfPrenom.setText(client.getPrenom());
			txfTel.setText(client.getTelephone());
			txfSexe.setText(client.getSexe());
			//Etat des boutons par defaut
			btnAjouter.setDisable(true);
			btnModifier.setDisable(false);
			btnSupprimer.setDisable(false);
			btnReset.setDisable(false);
			
		}
		
	}
	
	/**
	 * methode pour initialiser les champs de saisies
	 */
	private void initChamp() {
		txfNom.setText("");
		txfPrenom.setText("");
		txfSexe.setText("");
		txfTel.setText("");
	}
	
	/**
	 * methode permettant d'ajouter un enregistrement dans la table
	 */
	@FXML
	private void clickSurBtnAjouter() {
		
		if (txfNom.getText().equals("")) {
			
			alert=new Alert(AlertType.WARNING);
			alert.setHeaderText("ERREUR");
			alert.setContentText("Oups! Veuillez entrer le nom avant de continuer");
			alert.showAndWait();
			
		}
		else {
			
			if (service.create(new Client(null, txfNom.getText(), txfPrenom.getText(), txfSexe.getText(), txfTel.getText()))) {
				
				auto.getListClient().add(service.findLastElement());
				
				//message d'alert
				alert=new Alert(AlertType.INFORMATION);
				alert.initOwner(auto.getStage());
				alert.setHeaderText("ENREGISTREMENT");
				alert.setContentText("Votre enregistrement a �t� effectu� avec succ�s");
				alert.showAndWait();
				
				initChamp();
				
			}
			
		}
	
	}
	
	/**
	 * methode permettant de modifier un enregistrement
	 */
	@FXML
	private void clickSurBtnModifier() {
		
		int index=tableClient.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			if (txfNom.getText().equals("")) {
				
				alert=new Alert(AlertType.WARNING);
				alert.setHeaderText("ERREUR");
				alert.setContentText("Oups! Veuillez entrer le nom avant de continuer ce traitement");
				alert.showAndWait();
				
			} else {
				
				Client client=auto.getListClient().get(index);
				
				client.setNom(txfNom.getText());
				client.setPrenom(txfPrenom.getText());
				client.setSexe(txfSexe.getText());
				client.setTelephone(txfTel.getText());
				
				if (service.update(client)) {
					
					auto.getListClient().set(index, client);
					
					//message d'alert
					alert=new Alert(AlertType.INFORMATION);
					alert.initOwner(auto.getStage());
					alert.setHeaderText("MODIFICATION");
					alert.setContentText("Votre modification a �t� effectu�e avec succ�s");
					alert.showAndWait();
					
					initChamp();
					
					//changement d'etats des boutons
					btnAjouter.setDisable(false);
					btnModifier.setDisable(true);
					btnSupprimer.setDisable(true);
					btnReset.setDisable(true);
					
				}
				
			}	
			
		}
		
	}
	
	/**
	 * methode permettant de supprimer un enregistrement
	 * 
	 */
	@FXML
	private void clickSurBtnSupprimer() {
		
		int index=tableClient.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			alert=new Alert(AlertType.CONFIRMATION);
			alert.setHeaderText("ATTENTION");
			alert.setContentText("Voulez-vous supprimer ce enregistrement");
			
			Optional<ButtonType> option=alert.showAndWait();
			
			if(option.get()==ButtonType.OK) {
				
				if(service.delete(auto.getListClient().get(index).getId())) {
					
					auto.getListClient().remove(index);
					
					//message d'alert
					alert=new Alert(AlertType.INFORMATION);
					alert.initOwner(auto.getStage());
					alert.setHeaderText("SUPPRESSION");
					alert.setContentText("Votre suppression a �t� effectu� avec succ�s");
					alert.showAndWait();
					
					index=tableClient.getSelectionModel().getSelectedIndex();
					
					if (index<0) {
						
						initChamp();
						
						//changement d'etat des boutons;
						btnAjouter.setDisable(false);
						btnModifier.setDisable(true);
						btnReset.setDisable(true);
						btnSupprimer.setDisable(true);
						
					}
					
				} 
				
			}	else {
				
				alert=new Alert(AlertType.INFORMATION);
				alert.setHeaderText("ANNULATION");
				alert.setContentText("Suppression annul�e");
				alert.showAndWait();
				
			}	
			
		}
			
	}
	
	/**
	 * methode permettant de faire un reset
	 */
	@FXML
	private void clickSurReset() {
		
		initChamp();
		
		//changement des etats des boutons
		btnAjouter.setDisable(false);
		btnModifier.setDisable(true);
		btnSupprimer.setDisable(true);
		btnReset.setDisable(true);
		
		//supprimer la selection
		tableClient.getSelectionModel().clearSelection();
		
	}
	
}
