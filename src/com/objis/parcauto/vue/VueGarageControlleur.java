package com.objis.parcauto.vue;

import java.util.Optional;

import com.objis.parcauto.ParcAuto;
import com.objis.parcauto.domaine.Garage;
import com.objis.parcauto.service.impl.GarageService;
import com.objis.parcauto.service.impl.VoitureService;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class VueGarageControlleur {
	
	//les proprietes
	private ParcAuto auto;
	private GarageService service=new GarageService();
	private VoitureService serviceVoiture=new VoitureService();
	private Alert alert;
	
	@FXML
	private TextField txfNom;
	@FXML
	private TextField txfLocalisation;
	
	@FXML
	private Button btnAjouter;
	@FXML
	private Button btnModfier;
	@FXML
	private Button btnSupprimer;
	@FXML
	private Button btnReset;
	
	@FXML
	private TableView<Garage> tableGarage;
	@FXML
	private TableColumn<Garage, String> columNom;
	@FXML
	private TableColumn<Garage, String> columLocalisation;
	
	/**
	 * methode initialisation de la vue fxml
	 */
	@FXML
	public void initialize() {
		
		columNom.setCellValueFactory(e->e.getValue().nomProperty());
		columLocalisation.setCellValueFactory(e->e.getValue().localisationProperty());
		
		//remplissage des champs de saisies lorsqu'on click sur une ligne du tableau
		tableGarage.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->changerValuesChamps(newValue));
		
		//Etat des boutons par defaut
		btnAjouter.setDisable(false);
		btnModfier.setDisable(true);
		btnReset.setDisable(true);
		btnSupprimer.setDisable(true);
		
	}
	
	/**
	 * methode permettant de modifier la classe principale
	 * 
	 */
	public void setParAuto(ParcAuto auto) {
		this.auto=auto;
		tableGarage.setItems(auto.getListGarage());
	}
	
	/**
	 * methode permettant de changer les valeurs des champs de saisies
	 * 
	 */
	public void changerValuesChamps(Garage garage) {
		
		if (garage!=null) {
			
			txfLocalisation.setText(garage.getLocalisation());
			txfNom.setText(garage.getNom());
			
			//changement d'etats des boutons
			btnAjouter.setDisable(true);
			btnModfier.setDisable(false);
			btnSupprimer.setDisable(false);
			btnReset.setDisable(false);
			
		}
		
	}
	
	/**
	 * methode permettant de faire un enregistrement
	 */
	@FXML
	public void clickerSurBtnAjouter() {
		
		if (txfNom.getText().equals("")) {
			
			alert=new Alert(AlertType.WARNING);
			alert.setHeaderText("ERREUR");
			alert.setContentText("Oups! Veuillez entrer le nom avant de continuer ce traitement");
			alert.showAndWait();
			
		} else {
			
			if (service.create(new Garage(null, txfNom.getText(), txfLocalisation.getText()))) {
				
				this.auto.getListGarage().add(service.findLastElement());
				
				//message d'alert
				alert=new Alert(AlertType.INFORMATION);
				alert.initOwner(auto.getStage());
				alert.setHeaderText("ENREGISTREMENT");
				alert.setContentText("Votre enregistrement a �t� effectu� avec succ�s");
				alert.showAndWait();
				
				initChamps();
				
			}
			
		}	
		
	}
	
	/**
	 * methode permettant de modifier un enregistrement
	 */
	@FXML
	public void clickerSurBtnModfier() {
		
		int index=tableGarage.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			if (txfNom.getText().equals("")) {
				
				alert=new Alert(AlertType.WARNING);
				alert.setHeaderText("ERREUR");
				alert.setContentText("Oups! Veuillez entrer le nom avant de continuer ce traitement");
				alert.showAndWait();
				
			} else {
				
				Garage garage=auto.getListGarage().get(index);
				
				garage.setLocalisation(txfLocalisation.getText());
				garage.setNom(txfNom.getText());
				
				if (service.update(garage)) {
					
					auto.getListGarage().set(index, garage);
					
					//message d'alerte
					alert=new Alert(AlertType.INFORMATION);
					alert.initOwner(auto.getStage());
					alert.setHeaderText("MODIFICATION");
					alert.setContentText("Modification effectu�e avec succ�s");
					alert.showAndWait();
					
					initChamps();
					
					//changement d'etat des boutons
					btnAjouter.setDisable(false);
					btnModfier.setDisable(true);
					btnReset.setDisable(true);
					btnSupprimer.setDisable(true);
					
					//deselectionner la ligne selectionn�
					tableGarage.getSelectionModel().clearSelection();
					
				}
				
			}
					
		}
		
	}
	
	/**
	 * methode permettant de supprimer un enregistrement
	 */
	@FXML
	public void clickerSurBtnSupprimer() {
		
		int index=tableGarage.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			alert=new Alert(AlertType.CONFIRMATION);
			alert.setHeaderText("ATTENTION");
			alert.setContentText("Voulez-vous supprimer ce enregistrement");
			
			Optional<ButtonType> option=alert.showAndWait();
			
			if (option.get()==ButtonType.OK) {
				
				//verification de l'existance du garage dans voiture avant de procoder � sa suppression
				if(serviceVoiture.verifIdGarage(auto.getListGarage().get(index).getId())) {
					
					alert=new Alert(AlertType.ERROR);
					alert.setHeaderText("ERREUR");
					alert.setContentText("Cette suppression est impossible car ce garage est d�� associ� � une voiture");
					alert.showAndWait();
					
				} else {
					
					if (service.delete(auto.getListGarage().get(index).getId())) {
						
						auto.getListGarage().remove(index);
						
						//message d'alerte
						alert=new Alert(AlertType.INFORMATION);
						alert.initOwner(auto.getStage());
						alert.setHeaderText("SUPPRESSION");
						alert.setContentText("Supression effectu�e avec succ�s");
						alert.showAndWait();
						
						index=tableGarage.getSelectionModel().getSelectedIndex();
						
						if (index<0) {
							//chanegement d'etats des boutons
							btnAjouter.setDisable(false);
							btnModfier.setDisable(true);
							btnReset.setDisable(true);
							btnSupprimer.setDisable(true);
							
							initChamps();
						}
						
					}
					
				}
				
				
			} else {
				
				alert=new Alert(AlertType.INFORMATION);
				alert.setHeaderText("ANNULATION");
				alert.setContentText("Suppression annul�e");
				alert.showAndWait();
				
			}
				
		}
			
	}
	
	/**
	 * methode permettant de faire un reset
	 */
	@FXML
	public void clickerSurBtnReset() {
		
		initChamps();
		
		//changement d'etats des boutons
		btnAjouter.setDisable(false);
		btnModfier.setDisable(true);
		btnReset.setDisable(true);
		btnSupprimer.setDisable(true);
		
		//deselectionne la ligne selectionn�
		tableGarage.getSelectionModel().clearSelection();
		
	}
	
	/**
	 * methode pour initialiser les champs de saisies
	 */
	public void initChamps() {
		txfLocalisation.setText("");
		txfNom.setText("");
	}
		

}
