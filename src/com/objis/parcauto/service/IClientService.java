package com.objis.parcauto.service;

import com.objis.parcauto.domaine.Client;

public interface IClientService extends IService<Client,Long> {

}
