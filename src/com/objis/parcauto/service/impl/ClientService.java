package com.objis.parcauto.service.impl;

import java.util.List;

import com.objis.parcauto.dao.impl.ClientDao;
import com.objis.parcauto.domaine.Client;
import com.objis.parcauto.service.IClientService;

public class ClientService implements IClientService {
	
	//les proprietes
	private ClientDao dao=new ClientDao();
	
	public ClientService() {
		super();
	}

	@Override
	public Boolean create(Client t) {
		
		if (!"".equals(t.getNom())) return dao.create(t);
		
		return null;
	}

	@Override
	public Client readOne(Long pk) {
		
		if (pk>0) return dao.readOne(pk);
		
		return null;
	}

	@Override
	public Boolean update(Client t) {
		
		if (!"".equals(t.getNom())) return dao.update(t);
		
		return null;
	}

	@Override
	public Boolean delete(Long pk) {
		
		if (pk>0) return dao.delete(pk);
		
		return null;
	}

	@Override
	public List<Client> readAll() {
		
		return dao.readAll();
		
	}

	@Override
	public Client findLastElement() {
		
		return dao.findLastElement();
	}

}
