package com.objis.parcauto.service.impl;

import java.util.List;

import com.objis.parcauto.dao.impl.VoitureDao;
import com.objis.parcauto.domaine.Voiture;
import com.objis.parcauto.service.IVoitureService;

public class VoitureService implements IVoitureService{
	
	//les proprietes
	private VoitureDao dao=new VoitureDao();
	
	public VoitureService() {
		super();
	}

	@Override
	public Boolean create(Voiture t) {
		
		if (!"".equals(t.getImmatriculation())) return dao.create(t);
		
		return null;
	}

	@Override
	public Voiture readOne(Long pk) {
		
		if (pk>0) return dao.readOne(pk);
		
		return null;
	}

	@Override
	public Boolean update(Voiture t) {
		
		if (!"".equals(t.getMarque())) return dao.update(t);
		
		return null;
	}

	@Override
	public Boolean delete(Long pk) {
		
		if (pk>0) return dao.delete(pk);
		
		return null;
	}

	@Override
	public List<Voiture> readAll() {
		
		return dao.readAll();
		
	}

	@Override
	public Voiture findLastElement() {
		
		return dao.findLastElement();
		
	}
	
	/**
	 * methode permettant de verifier l'existance d'un chauffeur dans la table voiture
	 */
	public Boolean verifIdChauffeur(Long pk) {
		
		List<Voiture> listeVoiture=dao.readAll();
		
		for (Voiture voiture : listeVoiture) {
			
			if(voiture.getIdChauffeur().equals(pk)) {
				
				return true;
				
			}
			
		}
		
		
		return false;
	}
	
	/**
	 * methode permettant de verifier l'existance d'un garage dans la table voiture
	 */
	public Boolean verifIdGarage(Long pk) {
		
		List<Voiture> listeVoiture=dao.readAll();
		
		for (Voiture voiture : listeVoiture) {
			
			if (voiture.getIdGarage().equals(pk)) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}

}
