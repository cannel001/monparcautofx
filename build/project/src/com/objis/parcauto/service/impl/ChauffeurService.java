package com.objis.parcauto.service.impl;

import java.util.List;

import com.objis.parcauto.dao.impl.ChauffeurDao;
import com.objis.parcauto.domaine.Chauffeur;
import com.objis.parcauto.domaine.Voiture;
import com.objis.parcauto.service.IChauffeurService;

public class ChauffeurService implements IChauffeurService {
	
	//les proprietes
	private ChauffeurDao dao=new ChauffeurDao();
	
	public ChauffeurService() {
		super();
	}

	@Override
	public Boolean create(Chauffeur t) {
		
		if(!"".equals(t.getNomProperty())) {
			return dao.create(t);
		}
		
		return null;
	}

	@Override
	public Chauffeur readOne(Long pk) {
		
		if (pk>0) {
			return dao.readOne(pk);
		}
		
		return null;
	}

	@Override
	public Boolean update(Chauffeur t) {
		
		if (!"".equals(t.getNomProperty())){
			return dao.update(t);
		}
		
		return null;
	}

	@Override
	public Boolean delete(Long pk) {
		
		if (pk>0) {
			return dao.delete(pk);
		}
		
		return null;
	}

	@Override
	public List<Chauffeur> readAll() {
		
		return dao.readAll();
	
	}

	@Override
	public Chauffeur findLastElement() {
		
		return dao.findLastChauffeur();
		
	}
	
	

}
