package com.objis.parcauto.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class GenerateurEtatPdf<T> {
	
	//les proprietes
	private String userDirectory;
	private String outputScream;
	private List<T> maListe;
	private Map<String, Object> parameters;
	private JRBeanCollectionDataSource dataSource;
	private JasperPrint jasperPrint;
	private JasperReport report;
	private String nomFichier;
	private String nomDataSource;
	
	//contructeur
	public GenerateurEtatPdf() {
		// TODO Auto-generated constructor stub
	}
	
	//contructeur avec parametre
	public GenerateurEtatPdf(List<T> listDonnee,String nomFichier){
		
		this.maListe=listDonnee;
		this.nomFichier=nomFichier;
		
	}
	
	
	//methode permettant de generer le document
	public Boolean generate() {
		
		try {
			
			//recuperation du fichier du repertoire de user
			userDirectory=System.getProperty("user.home");
			
			//lien de destination du fichier pdf
			outputScream=userDirectory+File.separator+nomFichier+".pdf";
			
			//instanciation de la liste des donn�es
			maListe=new LinkedList<T>();
			
			//convertion de la liste en donn�es jasper
			dataSource=new JRBeanCollectionDataSource(maListe);
			
			//instanciation de la liste Map des parametres
			parameters=new HashMap<>();
			//ajout des parametres
			parameters.put(nomDataSource,dataSource);
			
			//generation de document pdf
			jasperPrint=JasperFillManager.fillReport(report, parameters, dataSource);
			
			//flux de sortie
			OutputStream sortieOutputStream=new FileOutputStream(new File(outputScream));
			
			//export du document
			JasperExportManager.exportReportToPdfStream(jasperPrint, sortieOutputStream);
			
			return true;
			
		} catch (JRException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
