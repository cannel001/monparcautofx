package com.objis.parcauto.service.impl;

import java.util.List;

import com.objis.parcauto.dao.impl.GarageDao;
import com.objis.parcauto.domaine.Garage;
import com.objis.parcauto.service.IGarageService;

public class GarageService implements IGarageService {
	
	//les proprietes
	GarageDao dao=new GarageDao();
	
	public GarageService() {
		super();
	}

	@Override
	public Boolean create(Garage t) {
		if (!"".equals(t.getNom())) return dao.create(t);
		return null;
	}

	@Override
	public Garage readOne(Long pk) {
		if (pk>0) return dao.readOne(pk);
		return null;
	}

	@Override
	public Boolean update(Garage t) {
		if (!"".equals(t.getNom())) return dao.update(t);
		return null;
	}

	@Override
	public Boolean delete(Long pk) {
		if (pk>0) return dao.delete(pk);
		return null;
	}

	@Override
	public List<Garage> readAll() {
		return dao.readAll();
	}

	@Override
	public Garage findLastElement() {
		return dao.findLastElement();
	}

}
