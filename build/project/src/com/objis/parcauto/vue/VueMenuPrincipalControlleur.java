package com.objis.parcauto.vue;

import java.util.Optional;

import com.objis.parcauto.ParcAuto;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;

public class VueMenuPrincipalControlleur {
	
	//les proprietes
	private ParcAuto parcauto;
	
	@FXML
	private MenuItem sousMenuClose;
	
	
	@FXML
	public void initialize() {
		
	}
	
	/**
	 * methode permettant de fermer l'application lorsqu'on click sur le sous menu close
	 */
	@FXML
	public void clickerSurSousMenuClose() {
		
		Alert alert=new Alert(AlertType.CONFIRMATION);
		alert.setHeaderText("FERMETURE");
		alert.setContentText("Voulez-vous fermer l'application");
		Optional<ButtonType> option=alert.showAndWait();
		
		if (option.get()==ButtonType.OK) {
			
			System.exit(10);
			
		} else {
			
			alert=new Alert(AlertType.INFORMATION);
			alert.setHeaderText("ANNULATION");
			alert.setContentText("Fermeture annul�e");
			alert.showAndWait();
			
		}
		
	}
	
	public void setParcAuto(ParcAuto auto) {
		this.parcauto=auto;
	}

}
