package com.objis.parcauto.vue;

import java.util.Optional;

import com.objis.parcauto.ParcAuto;
import com.objis.parcauto.domaine.Chauffeur;
import com.objis.parcauto.domaine.Garage;
import com.objis.parcauto.domaine.Voiture;
import com.objis.parcauto.service.impl.VoitureService;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class VueVoitureControlleur {
	
	//les proprietes
	private ParcAuto parcAuto;
	private VoitureService service=new VoitureService();
	private Alert alert;
	
	@FXML
	private TableView<Voiture> tableVoiture;
	
	@FXML
	private TableColumn<Voiture, String> columImmatriculation;
	@FXML
	private TableColumn<Voiture, String> columCouleur;
	@FXML
	private TableColumn<Voiture, String> columMarque;
	
	@FXML
	private TextField txfImmatriculation;
	@FXML
	private TextField txfCouleur;
	@FXML
	private TextField txfMarque;
	
	@FXML
	private Button btnAjouter;
	@FXML
	private Button btnModifier;
	@FXML
	private Button btnSupprimer;
	@FXML
	private Button btnReset;
	
	@FXML
	private ComboBox<Garage> cbxGarage;
	@FXML
	private ComboBox<Chauffeur> cbxChauffeur;
	
	
	@FXML
	public void initialize() {
		
		columCouleur.setCellValueFactory(e->e.getValue().couleurProperty());
		columImmatriculation.setCellValueFactory(e->e.getValue().immatriculationProperty());
		columMarque.setCellValueFactory(e->e.getValue().marqueProperty());
		
		//changer les valeurs des champs de saidies lorsqu'on selectionne une ligne du tableau
		tableVoiture.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->chargerChamps(newValue));
		
		//initialisation des champs de saisies
		initChamps();
		
		//changement d'etat des boutons
		btnAjouter.setDisable(false);
		btnModifier.setDisable(true);
		btnReset.setDisable(true);
		btnSupprimer.setDisable(true);
		
	}
	
	/**
	 * methode permettant de faire un enregistrement
	 */
	@FXML
	public void clickerSurBtnAjouter() {
		
		if (txfCouleur.getText().equals("") || txfImmatriculation.getText().equals("") || txfMarque.getText().equals("") || cbxChauffeur.getSelectionModel().getSelectedIndex()<0 || cbxGarage.getSelectionModel().getSelectedIndex()<0) {
			
			alert=new Alert(AlertType.WARNING);
			alert.setHeaderText("ERREUR");
			alert.setContentText("Oups! Veuillez renseigner tous les champs de saisies dans de continuer svp");
			alert.showAndWait();
			
		} else {
			
			if (service.create(new Voiture(null, cbxChauffeur.getSelectionModel().getSelectedItem().getIdProperty(), cbxGarage.getSelectionModel().getSelectedItem().getId(), txfImmatriculation.getText(), txfCouleur.getText(), txfMarque.getText()))){
				
				parcAuto.getListVoiture().add(service.findLastElement());
				
				//message d'alert
				alert=new Alert(AlertType.INFORMATION);
				alert.setHeaderText("INFORMATION");
				alert.setContentText("Votre enregistrement a �t� effectu� avec succ�s");
				alert.showAndWait();
				
				//initialisation des champs
				initChamps();
				
				//deselectionner les combo box
				cbxChauffeur.getSelectionModel().clearSelection();
				cbxGarage.getSelectionModel().clearSelection();
				
			}
			
		}	
			
	}
	
	/**
	 * methode permettant de modifier un enregistrement
	 */
	@FXML
	public void clickerSurBtnModfier() {
		
		int index=tableVoiture.getSelectionModel().getSelectedIndex();
		
		if(index>-1) {
			
			if(txfCouleur.getText().equals("") || txfImmatriculation.getText().equals("") || txfMarque.getText().equals("") || cbxChauffeur.getSelectionModel().getSelectedIndex()<0 || cbxGarage.getSelectionModel().getSelectedIndex()<0) {
				
				alert=new Alert(AlertType.WARNING);
				alert.setHeaderText("ERREUR");
				alert.setContentText("Oups! Veuillez renseigner tous les champs de saisies avant de continuer svp");
				alert.showAndWait();
				
			} else {
				
				Voiture voiture=parcAuto.getListVoiture().get(index);
				
				voiture.setCouleur(txfCouleur.getText());
				voiture.setIdChauffeur(cbxChauffeur.getSelectionModel().getSelectedItem().getIdProperty());
				voiture.setIdGarage(cbxGarage.getSelectionModel().getSelectedItem().getId());
				voiture.setImmatriculation(txfImmatriculation.getText());
				voiture.setMarque(txfMarque.getText());
				
				if(service.update(voiture)) {
					
					parcAuto.getListVoiture().set(index, voiture);
					
					//message d'alert
					alert=new Alert(AlertType.INFORMATION);
					alert.setHeaderText("MODIFICATION");
					alert.setContentText("Votre modification a �t� effectu�e avec succ�s");
					alert.showAndWait();
					
					//initialisation des champs de saisies
					initChamps();
					
					//changement d'etat des boutons
					btnAjouter.setDisable(false);
					btnModifier.setDisable(true);
					btnReset.setDisable(true);
					btnSupprimer.setDisable(true);
					
					//deselectionner la ligne selectionn�
					tableVoiture.getSelectionModel().clearSelection();
					
					//deselectionner les combo box
					cbxChauffeur.getSelectionModel().clearSelection();
					cbxGarage.getSelectionModel().clearSelection();
					
				}
				
			}
					
		}
		
	}
	
	/**
	 * methode permettant de supprimer un enregistrement
	 */
	@FXML
	public void clickerSurBtnSupprimer() {
		
		int index=tableVoiture.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			alert=new Alert(AlertType.CONFIRMATION);
			alert.setHeaderText("ATTENTION");
			alert.setContentText("Voulez-vous Supprimer ce enregistrement");
			
			Optional<ButtonType> option=alert.showAndWait();
			
			if (option.get()==ButtonType.OK) {
				
				if (service.delete(parcAuto.getListVoiture().get(index).getIdVoiture())) {
					
					parcAuto.getListVoiture().remove(index);
					
					//message d'alert
					alert=new Alert(AlertType.INFORMATION);
					alert.setHeaderText("SUPPRESSION");
					alert.setContentText("Votre suppression a �t� effectu�e avec succ�s");
					alert.showAndWait();
					
					//verifier si la table est table
					index=tableVoiture.getSelectionModel().getSelectedIndex();
					
					if(index<0) {
						
						//initialisation des champs
						initChamps();
						
						//changement d'etats des boutons
						btnAjouter.setDisable(false);
						btnModifier.setDisable(true);
						btnReset.setDisable(true);
						btnSupprimer.setDisable(true);
						
						//deselectionner la ligne selectionn�
						tableVoiture.getSelectionModel().clearSelection();
						
						//deselectionner les combo box
						cbxChauffeur.getSelectionModel().clearSelection();
						cbxGarage.getSelectionModel().clearSelection();
					}
					
				}
				
			} else {
				
				alert=new Alert(AlertType.INFORMATION);
				alert.setHeaderText("ANNULATION");
				alert.setContentText("Suppression annul�e");
				alert.showAndWait();
				
			}
				
		}
		
	}
	
	/**
	 * methode permettant de faire un reset
	 */
	@FXML
	public void clickerSurBtnReset() {
		
		//initialiser les champs de saisies
		initChamps();
		
		//changer les etats des boutons
		btnAjouter.setDisable(false);
		btnModifier.setDisable(true);
		btnReset.setDisable(true);
		btnSupprimer.setDisable(true);
		
		//deselectionner les combo box
		cbxChauffeur.getSelectionModel().clearSelection();
		cbxGarage.getSelectionModel().clearSelection();
		
		//deselectionner la ligne du tableau selectionn�
		tableVoiture.getSelectionModel().clearSelection();
		
	}
	
	/**
	 * methode permettant d'initialiser les champs de saisies
	 */
	public void initChamps() {
		
		txfCouleur.setText("");
		txfImmatriculation.setText("");
		txfMarque.setText("");
		
	}
	
	
	/**
	 * methode permettant de charger les champs de saisies
	 */
	public void chargerChamps(Voiture voiture) {
		
		if (voiture!=null) {
			
			txfCouleur.setText(voiture.getCouleur());
			txfImmatriculation.setText(voiture.getImmatriculation());
			txfMarque.setText(voiture.getMarque());
			
			//selection de garage en fonction du vehicule
			for (Garage garage : parcAuto.getListGarage()) {
				
				if (garage.getId().equals(voiture.getIdGarage())) cbxGarage.getSelectionModel().select(garage);
				
			}
			
			//selection du chauffeur en fonction du vehicule
			for (Chauffeur chauffeur : parcAuto.getListChauffeur()) {
				
				if (chauffeur.getIdProperty().equals(voiture.getIdChauffeur())) cbxChauffeur.getSelectionModel().select(chauffeur);
				
			}
			
			//changer les etats des boutons
			btnAjouter.setDisable(true);
			btnModifier.setDisable(false);
			btnReset.setDisable(false);
			btnSupprimer.setDisable(false);
			
		}
		
	}
	
	
	/**
	 * methode permettant de modifier la classe parcauto
	 */
	public void setParcAuto(ParcAuto auto) {
		this.parcAuto=auto;
		tableVoiture.setItems(parcAuto.getListVoiture());
		
		cbxChauffeur.setItems(parcAuto.getListChauffeur());
		cbxGarage.setItems(parcAuto.getListGarage());
	}

}
