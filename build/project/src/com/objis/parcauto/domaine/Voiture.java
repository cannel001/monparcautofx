package com.objis.parcauto.domaine;

import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Voiture {
	
	//les proprietes
	private ObjectProperty<Long> idVoiture;
	private ObjectProperty<Long> idChauffeur;
	private ObjectProperty<Long> idGarage;
	private StringProperty immatriculation;
	private StringProperty couleur;
	private StringProperty marque;
	
	//les constructeurs
	//par defaut
	public Voiture() {
		super();
		// TODO Auto-generated constructor stub
	}

	//avec parametres
	public Voiture(Long idVoiture, Long idChauffeur, Long idGarage,
			String immatriculation, String couleur, String marque) {
		super();
		this.idVoiture = new SimpleObjectProperty<Long>(idVoiture);
		this.idChauffeur = new SimpleObjectProperty<Long>(idChauffeur);
		this.idGarage = new SimpleObjectProperty<Long>(idGarage);
		this.immatriculation = new SimpleStringProperty(immatriculation);
		this.couleur = new SimpleStringProperty(couleur);
		this.marque = new SimpleStringProperty(marque);
	}

	//les gettes et setters
	//id voiture
	public Long getIdVoiture() {
		return idVoiture.get();
	}

	public void setIdVoiture(Long idVoiture) {
		this.idVoiture.set(idVoiture);
	}
	
	public ObjectProperty<Long> voitureProperty() {
		return idVoiture;
	}

	//id chauffeur
	public Long getIdChauffeur() {
		return idChauffeur.get();
	}

	public void setIdChauffeur(Long idChauffeur) {
		this.idChauffeur.set(idChauffeur);
	}
	
	public ObjectProperty<Long> chauffeurProperty() {
		return idChauffeur;
	}

	//id garage
	public Long getIdGarage() {
		return idGarage.get();
	}

	public void setIdGarage(Long idGarage) {
		this.idGarage.set(idGarage);
	}
	
	public ObjectProperty<Long> garageProperty() {
		return idChauffeur;
	}

	//immatriculation
	public String getImmatriculation() {
		return immatriculation.get();
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation.set(immatriculation);
	}
	
	public StringProperty immatriculationProperty() {
		return immatriculation;
	}

	//couleur
	public String getCouleur() {
		return couleur.get();
	}

	public void setCouleur(String couleur) {
		this.couleur.set(couleur);
	}
	
	public StringProperty couleurProperty() {
		return couleur;
	}

	//marque
	public String getMarque() {
		return marque.get();
	}

	public void setMarque(String marque) {
		this.marque.set(marque);
	}
	
	public StringProperty marqueProperty() {
		return marque;
	}

	//methode de description toString
	@Override
	public String toString() {
		return "Voiture [idVoiture=" + idVoiture + ", idChauffeur=" + idChauffeur + ", idGarage=" + idGarage
				+ ", immatriculation=" + immatriculation + ", couleur=" + couleur + ", marque=" + marque + "]";
	}
	
	
	
	
	
	
	
	

}
