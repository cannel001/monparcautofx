package com.objis.parcauto.domaine;

import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Garage {
	
	//les proprietes
	private ObjectProperty<Long> id;
	private StringProperty nom;
	private StringProperty localisation;
	
	//les constructeurs
	//par defaut
	public Garage() {
		super();
	}

	//avec parametres
	public Garage(Long id, String nom, String localisation) {
		this.id = new SimpleObjectProperty<Long>(id);
		this.nom = new SimpleStringProperty(nom);
		this.localisation = new SimpleStringProperty(localisation);
	}

	//les getters et setters
	//id
	public ObjectProperty<Long> idProperty() {
		return id;
	}

	public Long getId() {
		return id.get();
	}
	
	public void setId(Long id) {
		this.id.set(id);
	}

	//nom
	public String getNom() {
		return nom.get();
	}

	public void setNom(String nom) {
		this.nom.set(nom);
	}
	
	public StringProperty nomProperty() {
		return nom;
	}

	//Localisation
	public String getLocalisation() {
		return localisation.get();
	}

	public void setLocalisation(String localisation) {
		this.localisation.set(localisation);
	}
	
	public StringProperty localisationProperty() {
		return localisation;
	}

	//methode de descrition toString
	@Override
	public String toString() {
		return getNom();
	}
	
}
