package com.objis.parcauto.domaine;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Chauffeur {
	
	//les proprietes
	private ObjectProperty<Long> idProperty;
	private StringProperty nomProperty;
	private StringProperty prenomProperty;
	
	//les constructeurs
	//par defaut
	public Chauffeur() {
		super();
	}
	
	//constructeur par parametre des property
	public Chauffeur(Long idProperty,String nomProperty,String prenomProperty) {
		this.idProperty=new SimpleObjectProperty<Long>(idProperty);
		this.nomProperty=new SimpleStringProperty(nomProperty);
		this.prenomProperty=new SimpleStringProperty(prenomProperty);
	}

	//les getters et setters
	//Id property
	public ObjectProperty<Long> idProperty() {
		return idProperty;
	}

	public Long getIdProperty() {
		return idProperty.get();
	}
	
	public void setIdProperty(Long id) {
		this.idProperty.set(id);
	}

	//nom property
	public StringProperty nomproperty() {
		return nomProperty;
	}

	public String getNomProperty() {
		return nomProperty.get();
	}
	
	public void setNomProperty(String nom) {
		this.nomProperty.set(nom);
	}

	//prenom property
	public StringProperty prenomProperty() {
		return prenomProperty;
	}
	
	public String getPrenomProperty() {
		return prenomProperty.get();
	}
	
	public void setPrenomProperty(String prenom) {
		this.prenomProperty.set(prenom);
	}

	//la methode de descriptions
	@Override
	public String toString() {
		return getNomProperty() + " " + getPrenomProperty();
	}
	
}
