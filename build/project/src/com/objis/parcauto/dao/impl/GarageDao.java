package com.objis.parcauto.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.objis.parcauto.domaine.Garage;

public class GarageDao extends Dao<Garage,Long> {
	
	//les proprietes
	private final static String createStatement="insert into garage (nom,localisation) values (?,?)";
	private final static String readOneStatement="select * from garage where id=?";
	private final static String readAllStatement="select * from garage";
	private final static String updateStatement="update garage set nom=?,localisation=? where id=?";
	private final static String deleteStatement="delete from garage where id=?";
	private final static String findLaststatement="select * from garage order by id desc limit 1";
	
	private int executeQuery=-1;

	@Override
	public Boolean create(Garage t) {
		
		try {
			
			createStatement(createStatement);
			
			ps.setString(1, t.getNom());
			ps.setString(2, t.getLocalisation());
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return executeQuery>0;
	}

	@Override
	public Garage readOne(Long pk) {
		
		Garage garage=null;
		
		try {
			
			createStatement(readOneStatement);
			
			ps.setLong(1, pk);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				garage=new Garage(rs.getLong("id"), rs.getString("nom"), rs.getString("localisation"));
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return garage;
	}

	@Override
	public List<Garage> readAll() {
		
		List<Garage> listeGarage=new LinkedList<>();
		
		createStatement(readAllStatement);
		
		try {
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				listeGarage.add(new Garage(rs.getLong("id"), rs.getString("nom"), rs.getString("localisation")));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listeGarage;
	}

	@Override
	public Boolean update(Garage t) {
		
		try {
			
			createStatement(updateStatement);
			
			ps.setString(1, t.getNom());
			ps.setString(2, t.getLocalisation());
			ps.setLong(3, t.getId());
			
			executeQuery=ps.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	@Override
	public Boolean delete(Long pk) {
		
		try {
			
			createStatement(deleteStatement);
			
			ps.setLong(1, pk);
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}
	
	/**
	 * methode permettant de retourner le dernier enregistrement
	 */
	public Garage findLastElement() {
		
		Garage garage=null;
		
		try {
			
			createStatement(findLaststatement);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				garage=new Garage(rs.getLong("id"), rs.getString("nom"), rs.getString("localisation"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return garage;
	}

}
