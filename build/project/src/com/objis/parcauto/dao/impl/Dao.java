package com.objis.parcauto.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.objis.parcauto.dao.IDao;

public abstract class Dao<T,PK> implements IDao<T, PK> {
	
	//les proprietes
	protected PreparedStatement ps;
	private Connection cx=Singleton.getInstance();
	
	//methode preparedStatement
	public PreparedStatement createStatement(String req) {
		try {
			ps=cx.prepareStatement(req);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ps;
	}

}
