package com.objis.parcauto.vue;

import java.util.Optional;

import com.objis.parcauto.ParcAuto;
import com.objis.parcauto.domaine.Chauffeur;
import com.objis.parcauto.service.impl.ChauffeurService;
import com.objis.parcauto.service.impl.VoitureService;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class VueChauffeurControlleur {
	
	//les proprietes
	private ParcAuto parcAuto=new ParcAuto();
	private ChauffeurService service=new ChauffeurService();
	private VoitureService serviceVoiture=new VoitureService();
	private Alert alert;
	
	@FXML
	private TableView<Chauffeur> tableChauffeur;
	@FXML
	private TableColumn<Chauffeur, String> columNom;
	@FXML
	private TableColumn<Chauffeur, String> columPrenom;
	
	@FXML
	private TextField txfNom;
	@FXML
	private TextField txfPrenom;
	
	@FXML
	private Button btnAjouter;
	@FXML
	private Button btnModifier;
	@FXML
	private Button btnSupprimer;
	@FXML
	private Button btnReset;
	
	
	@FXML
	public void initialize() {
		
		//changement des etats des boutons
		btnAjouter.setDisable(false);
		btnModifier.setDisable(true);
		btnSupprimer.setDisable(true);
		btnReset.setDisable(true);
		
		columNom.setCellValueFactory(e->e.getValue().nomproperty());
		columPrenom.setCellValueFactory(e->e.getValue().prenomProperty());
		
		//changer les valeurs de champs de saisies lorsqu'on click sur une ligne du tableau
		tableChauffeur.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->afficherValeur(newValue));
			
	}

	public void setParAuto(ParcAuto auto) {
		this.parcAuto=auto;
		tableChauffeur.setItems(parcAuto.getListChauffeur());
	}
	
	/**
	 * methode pour changer les valeurs des champs de saisies
	 */
	public void afficherValeur(Chauffeur chauffeur) {
		
		if (chauffeur!=null) {
			
			txfNom.setText(chauffeur.getNomProperty());
			txfPrenom.setText(chauffeur.getPrenomProperty());
			
			//changement des etats des boutons
			btnAjouter.setDisable(true);
			btnModifier.setDisable(false);
			btnSupprimer.setDisable(false);
			btnReset.setDisable(false);
		}
			
	}
	
	/**
	 * methode pour ajouter un enregistrement dans la table
	 */
	@FXML
	public void clickerSurAjouter() {
		
		if (txfNom.getText().equals("")) {
			
			alert=new Alert(AlertType.WARNING);
			alert.setHeaderText("Erreur");
			alert.setContentText("Oups! Veuillez entrer le nom avant de continuer ce traitement");
			alert.showAndWait();
			
		} else {
			
			if (service.create(new Chauffeur(null,txfNom.getText(), txfPrenom.getText()))) {
				
				parcAuto.getListChauffeur().add(service.findLastElement());
				
				//message pour notifier l'enregsitrement
				alert=new Alert(AlertType.INFORMATION);
				alert.initOwner(parcAuto.getStage());
				alert.setHeaderText("ENREGISTREMENT");
				alert.setContentText("Votre enregistrement a �t� effectu� avec succ�s");
				alert.showAndWait();
				
				initChamp();
				
			}else {
				
				//message pour notifier l'enregsitrement
				alert=new Alert(AlertType.ERROR);
				alert.initOwner(parcAuto.getStage());
				alert.setHeaderText("ECHEC");
				alert.setContentText("Oups! une erreur survenue pendant l'enregistrement");
				alert.showAndWait();
				
			}
			
		}
	
	}
	
	/**
	 * methode pour modfier un enregistrement dans la tablea
	 */
	@FXML
	public void clickerSurModifier() {
		
		int index=tableChauffeur.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			if (txfNom.getText().equals("")) {
				
				alert=new Alert(AlertType.WARNING);
				alert.setHeaderText("Erreur");
				alert.setContentText("Oups! Veuillez entrer le nom avant de continuer ce traitement svp");
				alert.showAndWait();
				
			} else {
				
				Chauffeur chauffeur1=parcAuto.getListChauffeur().get(index);
				
				chauffeur1.setNomProperty(txfNom.getText());
				chauffeur1.setPrenomProperty(txfPrenom.getText());
				
				if (service.update(chauffeur1)) {	
					
					parcAuto.getListChauffeur().set(index,chauffeur1);
					
					//message d'alert
					alert=new Alert(AlertType.INFORMATION);
					alert.initOwner(parcAuto.getStage());
					alert.setHeaderText("MODIFICATION");
					alert.setContentText("Votre modification a �t� effectu� avec succ�s");
					alert.showAndWait();
					
					initChamp();
					
					//changement d'etat des btn
					btnAjouter.setDisable(true);
					btnModifier.setDisable(false);
					btnSupprimer.setDisable(false);
					btnReset.setDisable(false);	
			    }
				
			}
				
		}
		
	}
	
	/**
	 * methode pour supprimer un enregistrement
	 */
	@FXML
	public void clickerSurSupprimer() {
		
		int index=tableChauffeur.getSelectionModel().getSelectedIndex();
		
		if (index>-1) {
			
			alert=new Alert(AlertType.CONFIRMATION);
			alert.setHeaderText("ATTENTION");
			alert.setContentText("Voulez-vous supprimer ce enregistrement");
			
			Optional<ButtonType> option=alert.showAndWait();
			
			if(option.get()==ButtonType.OK) {
				
				//verification de l'existance du chauffeur dans voiture avant de supprimer
				if (serviceVoiture.verifIdChauffeur(parcAuto.getListChauffeur().get(index).getIdProperty())) {
					
					alert=new Alert(AlertType.ERROR);
					alert.setHeaderText("ERREUR");
					alert.setContentText("Cette suppression est impossible car ce chauffeur est d�j� associ� � une voiture");
					alert.showAndWait();
					
				} else {
					
					if (service.delete(parcAuto.getListChauffeur().get(index).getIdProperty())) {
						
						parcAuto.getListChauffeur().remove(index);
						
						//message d'alert
						alert=new Alert(AlertType.INFORMATION);
						alert.initOwner(parcAuto.getStage());
						alert.setHeaderText("SUPPRESSION");
						alert.setContentText("Votre Suppression a �t� effectu� avec succ�s");
						alert.showAndWait();
						
						//changement d'etats des boutons et initialsation des champs de saisies si la table est vide
						index=tableChauffeur.getSelectionModel().getSelectedIndex();
						
						if (index<0) {
							
							initChamp();
							
							btnAjouter.setDisable(false);
							btnModifier.setDisable(true);
							btnSupprimer.setDisable(true);
							btnReset.setDisable(true);
							
						}
					
				     }
					
				}
				
			} else {
				
				alert=new Alert(AlertType.INFORMATION);
				alert.setHeaderText("ANNULATION");
				alert.setContentText("Suppression annul�e");
				alert.showAndWait();
				
			}
			
		}
		
	}
	
	/**
	 * methode pour initialiser les champs de saisies
	 */
	@FXML
	public void clickerSurReset() {
		
		initChamp();
		
		//changement d'etats des boutons
		btnAjouter.setDisable(false);
		btnModifier.setDisable(true);
		btnReset.setDisable(true);
		btnSupprimer.setDisable(true);
		
		//supprimer la selection
		tableChauffeur.getSelectionModel().clearSelection();
		
	}
	
	/**
	 * methode pour initialiser les champs de saisies
	 * 
	 */
	public void initChamp() {
		txfNom.setText("");
		txfPrenom.setText("");
	}


}
