package com.objis.parcauto.service;

import com.objis.parcauto.domaine.Garage;

public interface IGarageService extends IService<Garage, Long> {

}
