package com.objis.parcauto.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.objis.parcauto.domaine.Chauffeur;

/**
 * Classe ChauffeurDao
 * @author beta_
 *
 */

public class ChauffeurDao extends Dao<Chauffeur,Long> {
	
	//les proprietes
	private final static String createStatement="insert into chauffeur (nom,prenom) values (?,?)";
	private final static String readOneStatement="select * from chauffeur where id=?";
	private final static String readAllStatement="select * from chauffeur";
	private final static String updateStatement="update chauffeur set nom=?,prenom=? where id=?";
	private final static String deleteStatement="delete from chauffeur where id=?";
	private final static String findLaststatement="select * from chauffeur order by id desc LIMIT 1";
	
	private int executeQuery=-1;

	/**
	 * methode permettant d'enregistrer un nouveau chauffeur
	 */
	@Override
	public Boolean create(Chauffeur t) {
		
		try {
			
			createStatement(createStatement);
			
			ps.setString(1, t.getNomProperty());
			ps.setString(2, t.getPrenomProperty());
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	/**
	 * methode permettant de lire un enregistrement
	 */
	@Override
	public Chauffeur readOne(Long pk) {
		
		Chauffeur chauffeur=null;
		
		try {
			
			createStatement(readOneStatement);
			
			ps.setLong(1, pk);
			
			ResultSet rs =ps.executeQuery();
			
			while (rs.next()) {
				
				chauffeur=new Chauffeur(rs.getLong("id"), rs.getString("nom"), rs.getString("prenom"));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return chauffeur;
	}

	/**
	 * methode permettant de retourner tous les chauffeurs
	 */
	@Override
	public List<Chauffeur> readAll() {
		
		Chauffeur chauffeur;
		
		List<Chauffeur> listeChauffeurs=new LinkedList<>();
		
		try {
			
			createStatement(readAllStatement);
			ResultSet rs=ps.executeQuery();
			
			while (rs.next()) {
				
				chauffeur=new Chauffeur(rs.getLong("id"), rs.getString("nom"), rs.getString("prenom"));
				listeChauffeurs.add(chauffeur);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listeChauffeurs;
	}

	/**
	 * methode permettant de modifier un enregistrement
	 */
	@Override
	public Boolean update(Chauffeur t) {
		
		Long id=t.getIdProperty();
		String nom=t.getNomProperty();
		String prenom=t.getPrenomProperty();
		
		try {
			
			createStatement(updateStatement);
			
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setLong(3, id);
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	@Override
	public Boolean delete(Long pk) {
		
		try {
			
			createStatement(deleteStatement);
			ps.setLong(1, pk);
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}
	
	/**
	 * methode permettant de retourner le dernier enregistrement
	 * @return Chauffeur
	 */
	public Chauffeur findLastChauffeur() {
		
		Chauffeur chauffeur=null;
		
		createStatement(findLaststatement);
		
		try {
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				
				chauffeur=new Chauffeur(rs.getLong("id"), rs.getString("nom"), rs.getString("prenom"));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return chauffeur;
		
	}
	

}
