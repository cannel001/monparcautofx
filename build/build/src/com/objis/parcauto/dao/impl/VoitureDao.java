package com.objis.parcauto.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.objis.parcauto.domaine.Voiture;

public class VoitureDao extends Dao<Voiture,Long> {
	
	//les proprietes
	private final static String createStatement="insert into voiture (couleur,idChauffeur,idGarage,immatriculation,marque) values (?,?,?,?,?)";
	private final static String readOneStatement="select * from voiture where idVoiture=?";
	private final static String readAllStatement="select * from voiture";
	private final static String updateStatement="update voiture set couleur=?,idChauffeur=?,idGarage=?,immatriculation=?,marque=? where idVoiture=?";
	private final static String deleteStatement="delete from voiture where idVoiture=?";
	private final static String findLaststatement="select * from voiture order by idVoiture desc limit 1";
	
	private int executeQuery=-1;

	@Override
	public Boolean create(Voiture t) {
		
		createStatement(createStatement);
		
		try {
			
			ps.setString(1, t.getCouleur());
			ps.setLong(2, t.getIdChauffeur());
			ps.setLong(3, t.getIdGarage());
			ps.setString(4, t.getImmatriculation());
			ps.setString(5, t.getMarque());
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	@Override
	public Voiture readOne(Long pk) {
		
		Voiture voiture=null;
		
		try {
			
			createStatement(readOneStatement);
			
			ps.setLong(1, pk);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				
				voiture=new Voiture(rs.getLong("idVoiture"), rs.getLong("idChauffeur"), rs.getLong("idGarage"), rs.getString("immatriculation"), rs.getString("couleur"), rs.getString("marque"));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return voiture;
	}

	@Override
	public List<Voiture> readAll() {
		
		List<Voiture> listeVoitures=new LinkedList<>();
		
		try {
			
			createStatement(readAllStatement);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				
				listeVoitures.add(new Voiture(rs.getLong("idVoiture"), rs.getLong("idChauffeur"), rs.getLong("idGarage"), rs.getString("immatriculation"), rs.getString("couleur"), rs.getString("marque")));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return listeVoitures;
	}

	@Override
	public Boolean update(Voiture t) {
		
		try {
			
			createStatement(updateStatement);
			
			ps.setString(1, t.getCouleur());
			ps.setLong(2, t.getIdChauffeur());
			ps.setLong(3, t.getIdGarage());
			ps.setString(4, t.getImmatriculation());
			ps.setString(5, t.getMarque());
			ps.setLong(6, t.getIdVoiture());
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}

	@Override
	public Boolean delete(Long pk) {
		
		try {
			
			createStatement(deleteStatement);
			
			ps.setLong(1, pk);
			
			executeQuery=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return executeQuery>0;
	}
	
	/**
	 * methode permettant de retourner le dernier enregistrement
	 */
	public Voiture findLastElement() {
		
		Voiture voiture=null;
		
		try {
			
			createStatement(findLaststatement);
			
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				
				voiture=new Voiture(rs.getLong("idVoiture"), rs.getLong("idChauffeur"), rs.getLong("idGarage"), rs.getString("immatriculation"), rs.getString("couleur"), rs.getString("marque"));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return voiture;
		
	}
	
	

}
