package com.objis.parcauto.dao;

import java.util.List;

public interface IDao<T,PK> {
	
	public Boolean create(T t);
	public T readOne(PK pk);
	public List<T> readAll();
	public Boolean update(T t);
	public Boolean delete(PK pk);
	

}
